<?php

namespace Uptogo\Sdk;

use \GoogleMapsHelper\GoogleMapsHelper;
use \GoogleMapsHelper\Route;
use \GoogleMapsHelper\Address;
use \GoogleMapsHelper\Geolocation;
use \Uptogo\Sdk\Order;

class Uptogo {
  private $apiKey;
  private $urlBase = 'https://uptogo.com.br/app/api/';
  private $baseCalc;
  private $ceps;

  public function __construct($apiKey) {
    $this->setApiKey($apiKey);
    $this->updateBaseCalc();
    $this->updateCeps();
  }

  public function setApiKey($apiKey) {
    $this->apiKey = $apiKey;
  }

  private function networkData($urlComplement, $content = array()) {
    return json_decode(file_get_contents(
      $this->urlBase . $urlComplement,
      false,
      stream_context_create(
        array(
          'http' => array(
            'method' => 'POST',
            'header'=>  "Content-Type: application/json\r\n" .
                        "Accept: application/json\r\n" .
                        "chave:" . $this->apiKey,
            'content' => json_encode($content)
          )
        )
      )
    ), true);
  }

  private function updateBaseCalc() {
    $this->baseCalc = $this->networkData('Admin.php/getValores');
  }

  private function updateCeps() {
    $this->ceps = $this->networkData('Admin.php/getCeps');
    foreach ($this->ceps as $i => $cep) {
      $this->ceps[$i]['Inicio'] = (int) $cep['Inicio'];
      $this->ceps[$i]['Fim'] = (int) $cep['Fim'];
    }
  }

  public function validateCep($cep) {
    $cep = (int) str_replace('-', '', $cep);
    foreach($this->ceps as $i => $v) {
      if ($cep >= $v['Inicio'] && $cep <= $v['Fim']) {
        return true;
      }
    }
    return false;
  }

  private function validateCode($code) {
    return $this->networkData('Admin.php/validarCodigo', array(
      'tipo' => 1,
      'valor' => $code
    ));
  }

  public function calcExpress($route, $code = null) {
    $price = $this->baseCalc['valorPontoExpresso'];
    $price += ($this->baseCalc['unitarioKm'] * ($route->getDistance() / 1000)) +
    ($this->baseCalc['unitarioMinuto'] * ($route->getDuration() / 60));
    if ($price < $this->baseCalc['minimo']) {
      $price = $this->baseCalc['minimo'];
    }
    $order = new Order($route, $price);
    if ($code !== null) {
      $resultValidateCode = $this->validateCode($code);
      if ($resultValidateCode['valido']) {
        $order->setCode($code);
        $order->setPrice($order->getPrice() - ($order->getPrice() * $resultValidateCode['desconto']));
      }
    }
    return $order;
  }

  public function calcEcommerce($route, $code = null) {
    $price = $this->baseCalc['valorPontoEcommerce'];
    $referenceAddress = new Address();
    $referenceAddress->setGeolocation(new Geolocation(-23.5502351,-46.633981));
    $distance = GoogleMapsHelper::getDirectDistance(
      $referenceAddress, $route->getDestination()
    ) / 1000;
    if ($distance > $this->baseCalc['raioEcommerce']) {
      $price += ($distance - $this->baseCalc['raioEcommerce']) *
      $this->baseCalc['taxaKmExcedidoEcommerce'];
    }    
    $route->setDistance($distance);
    $route->setDuration(60 * 60 * 24 * 2);
    $order = new Order($route, $price);
    if ($code !== null) {
      $resultValidateCode = $this->validateCode($code);
      if ($resultValidateCode['valido']) {
        $order->setCode($code);
        $order->setPrice($order->getPrice() - ($order->getPrice() * $resultValidateCode['desconto']));
      }
    }
    return $order;
  }

  public function newExpress($order, $password = null) {
    $order->setApiKey($this->apiKey);
    $order->setPassword($password);
    $response = $this->networkData('Pedido.php/criar', $order->parse());
    if ($response['sucesso']) {
      $order->setId($response['id']);
    }
    return $order;
  }

  public function newEcommerce($order, $password = null) {
    $order->setApiKey($this->apiKey);
    $order->setPassword($password);
    $parsed = $order->parse();
    if (isset($parsed['Pedido']['pontos'][0])) {
      array_splice($parsed['Pedido']['pontos'], 0, 1);
    }
    $response = $this->networkData('Pedido.php/criarEcommerce', $parsed);
    if ($response['sucesso']) {
      $order->setId($response['pedidoIdTrack']);
    }
    return $order;
  }
}
