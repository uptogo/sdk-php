<?php

namespace Uptogo\Sdk;

use \GoogleMapsHelper\Route;
use \GoogleMapsHelper\Address;
use \Uptogo\Sdk\AddressParser;

class Order {
  private $id;
  private $route;
  private $price;
  private $apiKey;
  private $password;
  private $code;

  public function __construct($route, $price) {
    $this->setRoute($route);
    $this->setPrice($price);
  }

  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getRoute() {
    return $this->route;
  }

  public function setRoute($route) {
    $this->route = $route;
  }

  public function getPrice() {
    return $this->price;
  }

  public function setPrice($price) {
    $this->price = $price;
  }

  public function getPassword() {
    return $this->password;
  }

  public function setPassword($password) {
    $this->password = $password;
  }

  public function getCode() {
    return $this->code;
  }

  public function setCode($code) {
    $this->code = $code;
  }

  public function setApiKey($apiKey) {
    $this->apiKey = $apiKey;
  }

  public function parse() {
    $result = RouteParser::parse($this->getRoute(), $this->getPassword());
    $result['MetodoPagamento'] = 3;
    $result['SenhaRastreamento'] = $this->getPassword();
    $result['OtimizarRota'] = false;
    $result['objetos'] = array();
    $result['Valor'] = round($this->getPrice(), 2);
    $result['apiKey'] = $this->apiKey;
    $result['CupomDesconto'] = $this->getCode();
    return array('Pedido' => $result);
  }
}
