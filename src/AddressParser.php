<?php

namespace Uptogo\Sdk;

use \GoogleMapsHelper\Address;
use \Uptogo\Sdk\GeolocationParser;

class AddressParser {
  public static function parse($address = null) {
    if ($address !== null) {
      return array(
        'Endereco' => array(
          'Bairro' => $address->getDistrict(),
          'Cep' => $address->getZipCode(),
          'Cidade' => $address->getCity(),
          'Estado' => $address->getState(),
          'Formatado' => $address->getFormatted(),
          'Logradouro' => $address->getStreet(),
          'Numero' => $address->getNumber()
        ),
        'Localizacao' => GeolocationParser::parse($address->getGeolocation()),
        'Tarefa' => 'Manipular objeto de loja virtual'
      );
    } else {
      return array();
    }
  }
}
