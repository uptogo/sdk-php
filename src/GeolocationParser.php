<?php

namespace Uptogo\Sdk;

use \GoogleMapsHelper\Geolocation;

class GeolocationParser {
  public static function parse($geolocation = null) {
    if ($geolocation !== null) {
      return array(
        'Latitude' => round($geolocation->getLat(), 6),
        'Longitude' => round($geolocation->getLng(), 6)
      );
    } else {
      return array();
    }
  }
}
