<?php

namespace Uptogo\Sdk;

use \GoogleMapsHelper\Route;

class RouteParser {
  public static function parse($route = null, $password = null) {
    if ($route !== null) {
      $origin = AddressParser::parse($route->getOrigin());
      $destination = AddressParser::parse($route->getDestination());
      if ($route->getOrigin() !== null) {
        $origin['Label'] = 'A';
        $origin['SenhaRastreamento'] = $password === null ? '' : $password;
      }
      if ($route->getDestination() !== null) {
        $destination['Label'] = 'B';
        $destination['SenhaRastreamento'] = $password === null ? '' : $password;
      }
      return array(
        'Distancia' => round($route->getDistance() / 1000, 2),
        'Tempo' => round($route->getDuration() / 60, 2),
        'OtimizarRota' => false,
        'pontos' => array(
          0 => $origin,
          1 => $destination
        )
      );
    } else {
      return array();
    }
  }
}
